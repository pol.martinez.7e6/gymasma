import java.io.*
import java.nio.file.Path
import kotlin.io.path.*
import java.time.*

/**
 * @author Adrián Rodríguez
 * @param users: Variable global que és una mutable list dels usuaris del gimnàs.
 */
val users = mutableListOf<List<String>>()

/**
 * @author Adrián Rodríguez
 * @param userData: Arxiu on es troben tots els usuaris del gimnàs.
 */
val userData = Path("./src/main/kotlin/data/userData.txt")

/**
 * @author Adrián Rodríguez
 * @param menu: Mostra un menú amb diferents funcionalitats, com per exemple, crear un usuari, modificar-ho, bloquejar-ho o desblojer-ho o sortir del sistema.
 */
fun menu() {
    println("""
MENÚ:
        |1. Importar dades
        |2. Crear usuari
        |3. Fer backup
        |4. Sortir
        """.trimMargin())
}

/**
 * @author Adrián Rodríguez
 * @param eleccion: Funció que et permet seleccionar una opció del menú.
 * @return la funció retorna l'opció escollida.
 */
fun eleccion(): Int {
    val eleccio = readln().toInt()
    return eleccio
}

/**
 * @author Pol Martinez
 * @param readFiles: Aquesta funció llegeix per files un arxiu de text i aquests els mostra un per un.
 * @return la funció retorna tots els usuaris del gimnàs.
 */
fun readFiles(): MutableList<List<String>> {
    val path = Path("./src/main/kotlin/import/")
    val files : List<Path> = path.listDirectoryEntries()
    val totalUsers = mutableListOf<List<String>>()
    for (item in files) {
        totalUsers.add(file2List(item.toString()))
    }
    return totalUsers
}

/**
 * @author Adrián Rodríguez
 * @param file2List: Et permet separar per diferents caràcters un text importat en el programa, en aquest cas `";"` (*punt i coma*).
 * @return la funció retorna tots els usuaris del gimnàs separats on està el punt i coma.
 */
fun file2List(ruta: String): List<String> {
    val fitxer = File(ruta)
    val str = fitxer.readText()
    fitxer.delete()
    return str.split(";")
}

/**
 * @author Pol Martinez
 * @param splitList: En aquesta funció separa el text per "," (*comes*).
 * @return la funció retorna les dades dels usuaris separats per les comes.
 */
fun splitList(dades: MutableList<List<String>>): MutableList<List<String>> {
    for (fichero in dades) {
        for (usuarioFichero in fichero) {
            val element: List<String> = usuarioFichero.split(",")
            users.add(element)
        }
    }
    return users
}
/**
 * @author Pol Martínez
 * @param writeUserData: És la funció que importa els usuaris del fitxer import al fitxer data.
 */
fun writeUserData() {
    val file = File("./src/main/kotlin/data/userData.txt")
    val fileContent = file.readText()
    if (fileContent.isEmpty()) {
        var singleUser: MutableList<String>
        var cont = 1
        for (user in users) {
            singleUser = user.toMutableList()
            if (singleUser[5] != "true" && singleUser[4] != "false") {
                singleUser[0] = cont.toString()
                var singleUser = singleUser.toString().replace(", ", ";")
                singleUser = singleUser.replace("[", "").replace("]", "")
                singleUser = singleUser.replace("\n", "")
                file.appendText("$singleUser;\n")
                cont++
            }

        }
    } else {
        var singleUser: MutableList<String>
        var cont = lastId()
        for (user in users) {
            singleUser = user.toMutableList()
            if (singleUser[5] != "true" && singleUser[4] != "false") {
                singleUser[0] = cont.toString()
                var singleUser = singleUser.toString().replace(", ", ";")
                singleUser = singleUser.replace("[", "").replace("]", "")
                singleUser = singleUser.replace("\n", "")
                file.appendText("$singleUser;\n")
                cont++
            }

        }
    }
}

/**
 * @author Adrián Rodríguez
 * @param import: Importa les dades i els emmagatzema en el nou fitxer.
 * @return la funció retorna tots els usuaris del gimnàs.
 */
fun import() {
    val users = splitList(readFiles())
    writeUserData()
}

/**
 * @author Pol Martínez
 * @param readData: Funció que llegeix el arxiu userData.txt línia per línia del directori data.
 * @return la funció retorna totes les dades els usuaris del gimnàs.
 */
fun readData(file: Path): List<String> {
    return file.readLines()
}

/**
 * @author Pol Martínez
 * @param lastId: La funció que va a l'última posició del fitxer i recull l'id de l'usuari.
 * @return la funció retorna l'últim id del fitxer.
 */
fun lastId(): Int {
    val lastUser = (readData(userData)[readData(userData).size - 1])
    val id = lastUser.split(";").toMutableList()[0]
    return id.toInt()+1
}

/**
 * @author Adrián Rodríguez
 * @param crearUsuari: Aquesta funció emmagatzema les dades d'un nou usuari.
 */
fun crearUsuari() {
    val file = File("./src/main/kotlin/data/userData.txt")
    var user = mutableListOf<String>()
    val id = lastId()
    user.add(id.toString())
    println("Escriu el nom de l'usuari nou:")
    val nom = readln()
    user.add(nom)
    println("Escriu el telefon de l'usuari nou:")
    val telefon = readln()
    user.add(telefon)
    println("Escriu el correu electronic de l'usuari nou:")
    val correu = readln()
    user.add(correu)
    val actiu = "true"
    user.add(actiu)
    val blocked = "false"
    user.add(blocked)
    val createdUser = user.toString().replace("[", "").replace("]", "")
    file.appendText(createdUser)
}

/**
 * @author Adrián Rodríguez
 * @param backUp: permet efectuar una cópia de seguretat diàriament del gimnàs.
 */
fun backUp() {
    val date = LocalDate.now()
    val source = File("./src/main/kotlin/data/userData.txt")
    val destination = File("./src/main/kotlin/backup/${date}.txt")
    source.copyTo(destination, overwrite = true)
}

fun main() {
    import()
    do {
        menu()
        val eleccion = eleccion()
        when (eleccion) {
            1 -> import()
            2 -> crearUsuari()
            3 -> backUp()
        }
    } while (eleccion != 4)
}
