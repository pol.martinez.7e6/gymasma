# Programa *gymASMA*
## Funcionament
El programa que hem desenvolupat importa, mitjançant fitxers, les dades dels usuaris de l'antic gimnàs al nou programa. Per aquest nou sistema de gestió d'usuaris que permet donar d'alta nous usuaris i enregistrar canvis, hem utilitzat diferents directoris i diverses funcions. Els tres directoris són:
- `Import:` Directori que importa els usuaris del gimnàs.
- `Backups:` Directori que permet efectuar una cópia de seguretat diàriament del gimnàs.
- `Data:` Directori que conté un arxiu de text amb dades del gimnàs.

## Funcions
Aquesta és la llista amb les funciones creades per al nou sistema de gestió d'usuaris:
- *`menu:`* Mostra un menú amb diferents funcionalitats, com per exemple, crear un usuari, modificar-ho, bloquejar-ho o desblojer-ho o sortir del sistema.
- *`eleccion:`* Funció que et permet seleccionar una opció del menú.
- *`readFiles:`* Aquesta funció llegeix per files un arxiu de text i aquests els mostra un per un.
- *`file2List:`* Et permet separar per diferents caràcters un text importat en el programa, en aquest cas `";"` (*punt i coma*).
- *`splitList:`* En aquesta funció separa el text per "," (*comes*).
- *`writeUserData:`* És la funció que importa els usuaris del fitxer import al fitxer data.
- *`import:`* Importa les dades i els emmagatzema en el nou fitxer.
- *`readData:`* Funció que llegeix el arxiu userData.txt línia per línia del directori data.
- *`lastId:`* La funció que va a l'última posició del fitxer i recull l'id de l'usuari.
- *`crearUsuari:`* Aquesta funció emmagatzema les dades d'un nou usuari.
- *`backUp:`* permet efectuar una cópia de seguretat diàriament del gimnàs.
